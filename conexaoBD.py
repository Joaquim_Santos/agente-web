import pymysql.cursors

class operacoesBD(object):
    def inserir_dados(self, fingerprint_navegador, ip_interno, sistema_navegador, dimensoes_tela, data_hora, fuso_horario, idioma, time_zone_local, url_pagina, tipo_evento, versao_cliente):
        conexao = pymysql.connect(host='127.0.0.1', user='root', passwd='', db='agenteweb',cursorclass=pymysql.cursors.DictCursor)
        #Ordem de inserção:
        #sistema_navegador = [sistema operacional, navegador, versão navegador, plugins]
        #dimensoes_tela = string com altura, largura, profundidade da cor

        try:
            with conexao.cursor() as cursor:
                comandoSQL = "INSERT INTO usuario (fingerprint_navegador, ip_interno, sistema_operacional, navegador_nome, navegador_versao, navegador_plugins, dimensoes_tela, data_hora, fuso_horario, idioma, time_zone_local, url_pagina, tipo_evento, versao_cliente) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"
                cursor.execute(comandoSQL, (fingerprint_navegador, ip_interno, sistema_navegador[0], sistema_navegador[1], sistema_navegador[2], sistema_navegador[3], dimensoes_tela, data_hora, fuso_horario, idioma, time_zone_local, url_pagina, tipo_evento, versao_cliente))
                conexao.commit()
        except Exception as excecao:
            print("ocorreu uma exceção ao realizar a inserção de dados do usuário no Banco MySQL")
            print("Tipo da exceção:\n")
            print(type(excecao))
            print("Descrição da exceção:\n")
            print(excecao)
    
    def inserir_ip_privado(self, fingerprint_navegador, ip_privado, data_hora1, data_hora2):
        conexao = pymysql.connect(host='127.0.0.1', user='root', passwd='', db='agenteweb',cursorclass=pymysql.cursors.DictCursor)
        try:
            with conexao.cursor() as cursor:
                comandoSQL = "UPDATE usuario SET ip_privado = %s WHERE fingerprint_navegador = %s AND data_hora BETWEEN %s AND %s;"
                cursor.execute(comandoSQL, (ip_privado, fingerprint_navegador, data_hora1, data_hora2))
                conexao.commit()
        except Exception as excecao:
            print("ocorreu uma exceção ao realizar a inserção de dados do usuário no Banco MySQL")
            print("Tipo da exceção:\n")
            print(type(excecao))
            print("Descrição da exceção:\n")
            print(excecao)
    
    def inserir_ip_publico(self, fingerprint_navegador, ip_publico, data_hora1, data_hora2):
        conexao = pymysql.connect(host='127.0.0.1', user='root', passwd='', db='agenteweb',cursorclass=pymysql.cursors.DictCursor)
        try:
            with conexao.cursor() as cursor:
                comandoSQL = "UPDATE usuario SET ip_publico = %s WHERE fingerprint_navegador = %s AND data_hora BETWEEN %s AND %s;"
                cursor.execute(comandoSQL, (ip_publico, fingerprint_navegador, data_hora1, data_hora2))
                conexao.commit()
        except Exception as excecao:
            print("ocorreu uma exceção ao realizar a inserção de dados do usuário no Banco MySQL")
            print("Tipo da exceção:\n")
            print(type(excecao))
            print("Descrição da exceção:\n")
            print(excecao)

    def inserir_localizacao(self, fingerprint_navegador, coordenadas, data_hora1, data_hora2, precisao_localizacao):
        conexao = pymysql.connect(host='127.0.0.1', user='root', passwd='', db='agenteweb',cursorclass=pymysql.cursors.DictCursor)
        try:
            with conexao.cursor() as cursor:
                comandoSQL = "UPDATE usuario SET latitude = %s, longitude = %s, precisao_localizacao = %s WHERE fingerprint_navegador = %s AND data_hora BETWEEN %s AND %s;"
                cursor.execute(comandoSQL, (coordenadas[0], coordenadas[1], precisao_localizacao,fingerprint_navegador, data_hora1, data_hora2))
                conexao.commit()
        except Exception as excecao:
            print("ocorreu uma exceção ao realizar a inserção de dados do usuário no Banco MySQL")
            print("Tipo da exceção:\n")
            print(type(excecao))
            print("Descrição da exceção:\n")
            print(excecao)

    def inserir_id_externo(self, fingerprint_navegador, id_externo, tag):
        conexao = pymysql.connect(host='127.0.0.1', user='root', passwd='', db='agenteweb',cursorclass=pymysql.cursors.DictCursor)
        try:
            with conexao.cursor() as cursor:
                comandoSQL = "SELECT MAX(data_hora) AS maxima FROM usuario WHERE fingerprint_navegador = %s;"
                cursor.execute(comandoSQL, (fingerprint_navegador))
                conexao.commit()
                data_hora = cursor.fetchall()[0]['maxima']
                comandoSQL = "UPDATE usuario SET id_externo = %s, tag = %s WHERE fingerprint_navegador = %s AND data_hora = %s;"
                cursor.execute(comandoSQL, (id_externo, tag, fingerprint_navegador, data_hora))
                conexao.commit()
        except Exception as excecao:
            print("ocorreu uma exceção ao realizar a inserção de dados do usuário no Banco MySQL")
            print("Tipo da exceção:\n")
            print(type(excecao))
            print("Descrição da exceção:\n")
            print(excecao)
    
    def selecionar_todos_dados(self):
        conexao = pymysql.connect(host='127.0.0.1', user='root', passwd='', db='agenteweb',cursorclass=pymysql.cursors.DictCursor)
        try:
            with conexao.cursor() as cursor:
                comandoSQL = "SELECT fingerprint_navegador, ip_interno, ip_publico, ip_privado, sistema_operacional,navegador_nome, navegador_versao, navegador_plugins, dimensoes_tela, data_hora, fuso_horario, idioma, time_zone_local, url_pagina, tipo_evento, versao_cliente, latitude, longitude, precisao_localizacao, id_externo, tag FROM usuario ORDER BY data_hora DESC;"
                cursor.execute(comandoSQL)
                dados_retornados = cursor.fetchall()
                return dados_retornados
        except Exception as excecao:
            print("ocorreu uma exceção ao realizar a consulta de dados do usuário no Banco MySQL")
            print("Tipo da exceção:\n")
            print(type(excecao))
            print("Descrição da exceção:\n")
            print(excecao)