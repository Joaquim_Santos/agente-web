$(document).ready(function(){
	var table = $('#tabela-acessos').DataTable({
		 "language": {
			//"sUrl": "{{url_for('static', filename = 'media/br.js')}}"
			"sEmptyTable":   "Nenhum registro encontrado",
			"sProcessing":   "A processar...",
			"sLengthMenu":   "Mostrar _MENU_ registos",
			"sZeroRecords":  "Não foram encontrados resultados",
			"sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registos",
			"sInfoEmpty":    "Mostrando de 0 até 0 de 0 registos",
			"sInfoFiltered": "(filtrado de _MAX_ registos no total)",
			"sInfoPostFix":  "",
			"sSearch":       "Procurar:",
			"sUrl":          "",
			"oPaginate": {
				"sFirst":    "Primeiro",
				"sPrevious": "Anterior",
				"sNext":     "Seguinte",
				"sLast":     "Último"
			}
			
		},
        "pagingType": "full_numbers",
        "ordering": false
	});
	
	$('a.toggle-vis').on( 'click', function (e) {
		e.preventDefault();
 
		// Get the column API object
		var column = table.column( $(this).attr('data-column') );
 
		// Toggle the visibility
		column.visible( ! column.visible() );
    } );
    
    $.ajax({
        url: '/index/', //No servidor: http://200.131.220.164/agenteweb/index/
        contentType: "application/json; charset=utf-8",
        type: 'POST', 
        data: JSON.stringify("dados"),
        success: function(response) {
            table.clear().draw();
	        for (var i = 0; i < response['quantidade']; i++) {
		        table.row.add([response['dados'][i]['fingerprint_navegador'], response['dados'][i]['ip_interno'], response['dados'][i]['ip_publico'], response['dados'][i]['ip_privado'], response['dados'][i]['sistema_operacional'], response['dados'][i]['navegador_nome'], response['dados'][i]['navegador_versao'] , response['dados'][i]['navegador_plugins'], response['dados'][i]['dimensoes_tela'], response['dados'][i]['data_hora'] , response['dados'][i]['fuso_horario'], response['dados'][i]['idioma'], response['dados'][i]['time_zone_local'], response['dados'][i]['url_pagina'], response['dados'][i]['tipo_evento'] , response['dados'][i]['versao_cliente'], response['dados'][i]['coordenadas'], response['dados'][i]['precisao_localizacao'], response['dados'][i]['id_externo'], response['dados'][i]['tag']]).draw(false).node();
	        }
            console.log(response);
        },
        error: function(xhr, status, error) {
			var errorMessage = xhr.status + ': ' + xhr.statusText
         	//alert('Error - ' + errorMessage);
            console.log(errorMessage);
        }
    });
	
});

$('#tabela-acessos').on( 'page.dt', function (event) {
    event.returnValue=false;
    event.cancel = true;
    document.getElementById('tituloTabela').click();
} );