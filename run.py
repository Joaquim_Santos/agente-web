# -*- coding: utf-8 -*-
from flask import Flask, request, jsonify, render_template
import conexaoBD
import json
from decimal import Decimal
import datetime

app = Flask(__name__)
app.config.from_object('config')


@app.route("/", methods = ['POST'])
def home():
    dados = {}
    for chave in request.form:
        dados[chave] = request.form.getlist(chave)
    
    if 'sistema_opercacional' in request.form:
        fingerprint_navegador = int(dados['fingerprint_navegador'][0])
        ip_interno = dados['ip_interno'][0]
        sistema_navegador = []
        sistema_navegador.append(dados['sistema_opercacional'][0])
        sistema_navegador.append(dados['navegador_nome_versao'][0].split(' ')[0])
        sistema_navegador.append(dados['navegador_nome_versao'][0].split(' ')[1])
        try:
            navegador_plugins = ''
            for plugin in dados['navegador_plugins[]']:
                if navegador_plugins == '':
                    navegador_plugins = navegador_plugins + plugin
                else:
                    navegador_plugins = navegador_plugins + ' | ' + plugin
            sistema_navegador.append(navegador_plugins)
        except:
            sistema_navegador.append(None)
        
        dimensoes_tela = str(dados['dimensoes_tela[]'][0]) + ',' + str(dados['dimensoes_tela[]'][1]) + ',' + str(dados['dimensoes_tela[]'][2])
        data_hora = str(dados['data_hora'][0])
        fuso_horario = str(dados['fuso_horario'][0][25:])
        idioma = dados['idioma'][0]
        time_zone_local = dados['time_zone_local'][0]
        url_pagina = dados['url_pagina'][0]
        tipo_evento = dados['tipo_evento'][0]
        versao_cliente = dados['versao_cliente'][0]
        #print(fingerprint_navegador, ip_interno, sistema_navegador, navegador_plugins, dimensoes_tela, data_hora, idioma, time_zone_local, url_pagina, tipo_evento, versao_cliente, fuso_horario)
        
        data = data_hora.split(' ')[0]
        hora = data_hora.split(' ')[1]
        data = data.split('-')
        hora = hora.split(':')
        data_hora1 = str(datetime.datetime(int(data[0]),int(data[1]),int(data[2]), int(hora[0]),int(hora[1]),int(hora[2])))
        
        operacoes_banco = conexaoBD.operacoesBD()
        operacoes_banco.inserir_dados(fingerprint_navegador, ip_interno, sistema_navegador, dimensoes_tela, data_hora1, fuso_horario, idioma, time_zone_local, url_pagina, tipo_evento, versao_cliente)
    
    elif 'ip_privado' in request.form:
        fingerprint_navegador = int(dados['fingerprint_navegador'][0])
        data_hora = str(dados['data_hora'][0])
        ip_privado = dados['ip_privado'][0]
        #print(fingerprint_navegador, ip_privado, data_hora)
        
        data = data_hora.split(' ')[0]
        hora = data_hora.split(' ')[1]
        data = data.split('-')
        hora = hora.split(':')
        try:
            data_hora1 = datetime.datetime(int(data[0]),int(data[1]),int(data[2]), int(hora[0]),int(hora[1]),int(hora[2]))
            data_hora2 = datetime.datetime(int(data[0]),int(data[1]),int(data[2]), int(hora[0]),int(hora[1]),int(hora[2])+10)
        except:
            data_hora1 = datetime.datetime(int(data[0]),int(data[1]),int(data[2]), int(hora[0]),int(hora[1]),int('50'))
            data_hora2 = datetime.datetime(int(data[0]),int(data[1]),int(data[2]), int(hora[0]),int(hora[1]),int('59'))

        operacoes_banco = conexaoBD.operacoesBD()
        operacoes_banco.inserir_ip_privado(fingerprint_navegador, ip_privado, data_hora1, data_hora2)

    elif 'ip_publico' in request.form:
        fingerprint_navegador = int(dados['fingerprint_navegador'][0])
        data_hora = str(dados['data_hora'][0])
        ip_publico = dados['ip_publico'][0]
        #print(fingerprint_navegador, ip_publico, data_hora)
        
        data = data_hora.split(' ')[0]
        hora = data_hora.split(' ')[1]
        data = data.split('-')
        hora = hora.split(':')
        try:
            data_hora1 = datetime.datetime(int(data[0]),int(data[1]),int(data[2]), int(hora[0]),int(hora[1]),int(hora[2]))
            data_hora2 = datetime.datetime(int(data[0]),int(data[1]),int(data[2]), int(hora[0]),int(hora[1]),int(hora[2])+10)
        except:
            data_hora1 = datetime.datetime(int(data[0]),int(data[1]),int(data[2]), int(hora[0]),int(hora[1]),int('50'))
            data_hora2 = datetime.datetime(int(data[0]),int(data[1]),int(data[2]), int(hora[0]),int(hora[1]),int('59'))
        
        operacoes_banco = conexaoBD.operacoesBD()
        operacoes_banco.inserir_ip_publico(fingerprint_navegador, ip_publico, data_hora1, data_hora2)
        
    elif 'latitude' in request.form:
        fingerprint_navegador = int(dados['fingerprint_navegador'][0])
        data_hora = str(dados['data_hora'][0])
        latitude = dados['latitude'][0]
        longitude = dados['longitude'][0]
        precisao_localizacao = dados['precisao_localizacao'][0]
        #print(fingerprint_navegador, latitude, longitude, data_hora, precisao_localizacao)

        data = data_hora.split(' ')[0]
        hora = data_hora.split(' ')[1]
        data = data.split('-')
        hora = hora.split(':')
        try:
            data_hora1 = datetime.datetime(int(data[0]),int(data[1]),int(data[2]), int(hora[0]),int(hora[1]),int(hora[2]))
            data_hora2 = datetime.datetime(int(data[0]),int(data[1]),int(data[2]), int(hora[0]),int(hora[1]),int(hora[2])+10)
        except:
            data_hora1 = datetime.datetime(int(data[0]),int(data[1]),int(data[2]), int(hora[0]),int(hora[1]),int('50'))
            data_hora2 = datetime.datetime(int(data[0]),int(data[1]),int(data[2]), int(hora[0]),int(hora[1]),int('59'))
    
        operacoes_banco = conexaoBD.operacoesBD()
        operacoes_banco.inserir_localizacao(fingerprint_navegador, [latitude, longitude], data_hora1, data_hora2, precisao_localizacao)
       
    elif 'id_externo' in request.form:
        fingerprint_navegador = int(dados['fingerprint_navegador'][0])
        id_externo = dados['id_externo'][0]
        tag = dados['TAG'][0]
        #print(fingerprint_navegador, id_externo, tag)

        
        operacoes_banco = conexaoBD.operacoesBD()
        operacoes_banco.inserir_id_externo(fingerprint_navegador, id_externo, tag)
        
    return "DADOS COLETADOS!!!"

@app.route("/coleta/", methods = ['POST', 'GET'])
def coleta():
    return render_template("coleta.html")

@app.route("/index/", methods = ['POST', 'GET'])
def index():
    if request.method == 'POST':
        operacoes_banco = conexaoBD.operacoesBD()
        lista_acessos = operacoes_banco.selecionar_todos_dados()
        lista_acessos2 = {}
        lista_acessos3 = []
        coordenadas = []

        for posicao in range(0, len(lista_acessos)):
            for acesso in lista_acessos[posicao].keys():
                if acesso == 'dimensoes_tela':
                    dimensoes = str(lista_acessos[posicao][acesso]).split(',')
                    if dimensoes is not None:
                        lista_acessos2[acesso] = 'Altura: ' + str(dimensoes[0]) + ' | Largura: ' + str(dimensoes[1]) + ' | Nível de profundidade da cor: ' + str(dimensoes[2])
                    else:
                        lista_acessos2[acesso] = None
                elif acesso == 'latitude' or acesso == 'longitude':
                    coordenadas.append(lista_acessos[posicao][acesso])
                    if len(coordenadas) == 2:
                        if coordenadas[0] is not None:
                            lista_acessos2['coordenadas'] = 'Latitude: ' + str(coordenadas[0]) + ' | Longitude: ' + coordenadas[1]
                        else:
                            lista_acessos2['coordenadas'] = None
                else:
                    if lista_acessos[posicao][acesso] is not None:
                        lista_acessos2[acesso] = str(lista_acessos[posicao][acesso])
                    else:
                        lista_acessos2[acesso] = None

            lista_acessos3.append(lista_acessos2)
            lista_acessos2 = {}
            coordenadas = []
        return jsonify({'quantidade': len(lista_acessos3), 'dados': lista_acessos3})

    return render_template("index.html")

if __name__ == '__main__':
    app.run()

