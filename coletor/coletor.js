var versao_cliente = "1.0.0"
$(document).ready(function(){
    var tipo_evento = "Page_view"
    //alert("Fingerprint: " + fingerprint_navegador);
    /* IP Interno */
    try {
        var ip_interno = verificar_ip();
        if (ip_interno != false) {
            //alert("IP Interno:" + ip_interno);
        };
    }
    catch (e){
        console.log("Impossibilitado de requisitar IP interno externamente");
        ip_interno = '';  
    }
    /* Sistema e navegador */
    var sistema_operacional = verificar_sistema();
    //alert('Sistema Operacional: '+ verificar_sistema());
    var navegador_nome_versao = navigator.sayswho;
    //alert("Navegador e Versão: " + navigator.sayswho); 
    var dimensoes_tela = [screen.availHeight, screen.availWidth, screen.colorDepth];
    //alert("Altura da tela: " + screen.availHeight + " | Largura da Tela: " + screen.availWidth + " Profundadidade de cor: " + screen.colorDepth);
    
    /* Data, Hora, idioma */
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var data_atual = Date(Date.now()).toString();
    var data_hora = date + ' ' + time;
    data_hora2 = data_hora;
    //alert("Data e Hora Atual: " + date + " - "+ time);
    var fuso_horario = data_atual;
    //alert("Data e Fuso Horário: " + data_atual);
    var idioma = navigator.language;
    //alert("Idioma: " + navigator.language);
    
    /* Plugins Instalados */
    var navegador_plugins = [];
    for (var x = 0; x < navigator.plugins.length; x++){
        //alert("Plugin: " + navigator.plugins[x].name);
        navegador_plugins.push(navigator.plugins[x].name);
    }

    /* Obter timezone do usuário e URL atual da página*/
    timezone = jstz.determine()
    var time_zone_local = timezone.name();
    var url_pagina = window.location.href;
    
    $.ajax({
        type: 'POST',
        data: {'fingerprint_navegador' : fingerprint_navegador,'ip_interno': ip_interno, 'sistema_opercacional' : sistema_operacional, 'navegador_nome_versao' : navegador_nome_versao, 'dimensoes_tela' : dimensoes_tela, 'data_hora' : data_hora, 'fuso_horario' : fuso_horario, 'idioma' : idioma, 'navegador_plugins' : navegador_plugins, 'time_zone_local' : time_zone_local, 'url_pagina' : url_pagina, 'tipo_evento' : tipo_evento, 'versao_cliente' : versao_cliente},
        url : "http://127.0.0.1:5000",
        dataType: "json",

        success: function(response) {
            console.log(response);   
        },
        error: function(error) {
            console.log(error);
        }
    });


    //===================================================================================================
    /* IP Público*/  
    try {  
        $.getJSON("http://jsonip.com/?callback=?", function (data) {
            //alert("IP Público: " + data.ip);
            $.ajax({
                type: 'POST',
                data: {'ip_publico' : data.ip, 'fingerprint_navegador' : fingerprint_navegador, 'data_hora' : data_hora},
                url : "http://127.0.0.1:5000",
                dataType: "json",
        
                success: function(response) {
                    if(response == 1){
                        console.log(response);   
                    }
                    else{
                        console.log(response);   
                    }
                },
                error: function(error) {
                    console.log(error);
                }
            });
        });
    }
    catch (e){
        console.log("Impossibilitado de requisitar IP público externamente");
    }


    //===================================================================================================
    /* IP Privado*/  
    try{
        getUserIP(function(ip){
            //alert("IP Privado:" + ip);
            $.ajax({
                type: 'POST',
                data: {'ip_privado' : ip, 'fingerprint_navegador' : fingerprint_navegador, 'data_hora' : data_hora},
                url : "http://127.0.0.1:5000",
                dataType: "json",
        
                success: function(response) {
                    if(response == 1){
                        alert("Dados inseridos com sucesso!");
                        console.log(response);   
                    }
                    else{
                        alert("Erro ao inserir dados!");
                        console.log(response);   
                    }
                },
                error: function(error) {
                    console.log(error);
                }
            });
        });
    }
    catch (e) {
        console.log("Sem suporte no navegador para coleta de IP privado");
      }


    //===================================================================================================  
    /* Coordenadas */
    localizar_usuario();
    
});


//===================================================================================================

 /* IP Interno */
function verificar_ip() {
    if (window.XMLHttpRequest) xmlhttp = new XMLHttpRequest();
    else xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  
    xmlhttp.open("GET", "http://api.hostip.info/get_html.php", false);
    xmlhttp.send();
  
    hostipInfo = xmlhttp.responseText.split("\n");
    
    for (i = 0; i <= hostipInfo.length; i++) {
      ipAddress = hostipInfo[i].split(":");
      if (ipAddress[0] == "IP") return ipAddress[1].replace(' ', '');
    }
  
    return false;
  
  }


//===================================================================================================

/*  Obtendo Sistema Operacional  */ 
function verificar_sistema() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    var OSNome = "";
    /*  Windows  */
    if (window.navigator.userAgent.indexOf("Windows NT 10.0")!= -1){
        OSNome="Windows 10";
    }
    else if (window.navigator.userAgent.indexOf("Windows NT 6.2") != -1){ 
        OSNome="Windows 8";
    }
    else if (window.navigator.userAgent.indexOf("Windows NT 6.1") != -1){ 
        OSNome="Windows 7";
    }
    else if (window.navigator.userAgent.indexOf("Windows NT 6.0") != -1){ 
        OSNome="Windows Vista";
    }
    else if (window.navigator.userAgent.indexOf("Windows NT 5.2") != -1){ 
        OSNome="Windows Server 2003";
    }
    else if (window.navigator.userAgent.indexOf("Windows NT 5.1") != -1){ 
        OSNome="Windows XP";
    }
    else if (window.navigator.userAgent.indexOf("Windows NT 5.0") != -1){ 
        OSNome="Windows 2000";
    }
    else if (window.navigator.userAgent.indexOf("Windows ME") != -1){     
        OSNome="Windows ME";
    }
    else if (window.navigator.userAgent.indexOf("Windows NT 4.0") != -1){ 
        OSNome="Windows NT 4.0";
    }
    else if (window.navigator.userAgent.indexOf("Windows 95") != -1){     
        OSNome="Windows 95";
    }
    else if (window.navigator.userAgent.indexOf("Windows 98") != -1){     
        OSNome="Windows 98";
    }
    /*  Mobiles  */
    else if (/android/i.test(userAgent)) {
        OSNome="Android";
    }
    else if(/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        OSNome="IOS";
    }
    else if(/windows phone/i.test(userAgent)) {
        OSNome="Windows Phone";
    }
    /*  Mac OS  */
    else if (window.navigator.userAgent.indexOf("Macintosh") != -1){ 
        OSNome="Mac OS";
    }
    /*  Linux  */
    else if (window.navigator.userAgent.indexOf("X11") != -1){ 
        OSNome="UNIX";
    }
    else if (window.navigator.userAgent.indexOf("Linux") != -1){
        OSNome="Linux";
    }
    /*  Sun e Outros  */
    else if (window.navigator.userAgent.indexOf("OpenBSD") != -1){        
        OSNome="OpenBSD";
    }
    else if (window.navigator.userAgent.indexOf("SunOS") != -1){          
        OSNome="SunOS";
    }
    else if (window.navigator.userAgent.indexOf("QNX") != -1){            
        OSNome="QNX";
    }
    else if (window.navigator.userAgent.indexOf("BeOS") != -1){           
        OSNome="BeOS";
    }
    /*  Search Bots  */
    else if (window.navigator.userAgent.indexOf("nuhk") != -1){           
        OSNome="Search Bot";
    }
    else if (window.navigator.userAgent.indexOf("Googlebot") != -1){           
        OSNome="Search Bot";
    }
    else if (window.navigator.userAgent.indexOf("Yammybot") != -1){           
        OSNome="Search Bot";
    }
    else if (window.navigator.userAgent.indexOf("Openbot") != -1){           
        OSNome="Search Bot";
    }
    else if (window.navigator.userAgent.indexOf("Slurp") != -1){           
        OSNome="Search Bot";
    }
    else if (window.navigator.userAgent.indexOf("MSNBot") != -1){           
        OSNome="Search Bot";
    }
    else if (window.navigator.userAgent.indexOf("ia_archiver") != -1){           
        OSNome="Search Bot";
    }

    return OSNome;    
}


//===================================================================================================

 /*  Obtendo Navegador e Versão  */
navigator.sayswho= (function(){
    var ua= navigator.userAgent, tem, 
    M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    if(M[1]=== 'Chrome'){
        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
        if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
    return M.join(' ');
})();


//===================================================================================================

var data_hora2;
/*  Obtendo Latitude e Longitude  */
function localizar_usuario(){
    if (navigator.geolocation){
        navigator.geolocation.getCurrentPosition(buscar_posicao);
    }
    else{
        console.log("Sem suporte no navegador para geolocalização");
        return 0;
    }
}

function buscar_posicao(position) {
    //alert("Latitude: " + position.coords.latitude +
    //" Longitude: " + position.coords.longitude);
    
    $.ajax({
        type: 'POST',
        data: {'latitude' : position.coords.latitude, 'longitude': position.coords.longitude, 'fingerprint_navegador' : fingerprint_navegador, 'data_hora' : data_hora2, 'precisao_localizacao': position.coords.accuracy},
        url : "http://127.0.0.1:5000",
        dataType: "json",

        success: function(response) {
            if(response == 1){
                alert("Dados inseridos com sucesso!");
                console.log(response);   
            }
            else{
                alert("Erro ao inserir dados!");
                console.log(response);   
            }
        },
        error: function(error) {
            console.log(error);
        }
    });
  } 


//===================================================================================================

/*  Obter Fingerprint  */
var fingerprint_navegador = (function(window, screen, navigator) {

// https://github.com/darkskyapp/string-hash
function checksum(str) {
    var hash = 5381,
        i = str.length;

    while (i--) hash = (hash * 33) ^ str.charCodeAt(i);

    return hash >>> 0;
}

// http://stackoverflow.com/a/4167870/1250044
function map(arr, fn){
    var i = 0, len = arr.length, ret = [];
    while(i < len){
        ret[i] = fn(arr[i++]);
    }
    return ret;
}

return checksum([
    navigator.userAgent,
    [screen.height, screen.width, screen.colorDepth].join('x'),
    new Date().getTimezoneOffset(),
    !!window.sessionStorage,
    !!window.localStorage,
    map(navigator.plugins, function (plugin) {
        return [
            plugin.name,
            plugin.description,
            map(plugin, function (mime) {
                return [mime.type, mime.suffixes].join('~');
            }).join(',')
        ].join("::");
    }).join(';')
].join('###'));

}(this, screen, navigator));


//===================================================================================================

/**
 * Obter o IP Privado do usuário através da conexão webkitRTCPeerConnection
 * @param onNewIP {Function} Função de escuta, que mostra o IP localmente
 * @return undefined
 */
function getUserIP(onNewIP) { //  onNewIp - your listener function for new IPs
    var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
    var pc = new myPeerConnection({
        iceServers: []
    }),
    noop = function() {},
    localIPs = {},
    ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
    key;

    function iterateIP(ip) {
        if (!localIPs[ip]) onNewIP(ip);
        localIPs[ip] = true;
    }

    pc.createDataChannel("");

    pc.createOffer().then(function(sdp) {
        sdp.sdp.split('\n').forEach(function(line) {
            if (line.indexOf('candidate') < 0) return;
            line.match(ipRegex).forEach(iterateIP);
        });
        
        pc.setLocalDescription(sdp, noop, noop);
    }).catch(function(reason) {
        // Erro obtido, falha de conexão
    });

    pc.onicecandidate = function(ice) {
        if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return;
        ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
    };
}


